function beSelected(){
    
    if(exitIfTitleClicked(this)){
        event.preventDefault();  
        return;
    }
        
    
    if(!checkIfShiftKeyIsPressed(event)){
        clearSelected();
    }
    
    if(!checkIfHasClass(this, "selected"))                
        this.className = this.className + " selected";
    else
        this.className = ' ';    
    
    event.preventDefault();  
                     
}
                
function checkIfHasClass(link, cls){                
    return (' ' + link.className + ' ').indexOf(' ' + cls + ' ') > -1;
}

function exitIfTitleClicked(link){
    console.log(checkIfHasClass(link, "title"));
    if(checkIfHasClass(link, "title"))
        return true;
    else
        return false;    
}
          
function clearSelected(){
    var links = document.getElementsByTagName("a");     
    for (var i=0;i<links.length;i++) { 
        if(!checkIfHasClass(links[i], "title"))
            links[i].className = ' ';                    
    } 
}
          
function checkIfShiftKeyIsPressed(){
    return event.shiftKey;
}
           
function addCountryToSelect(){
    var country = document.getElementById("country").value;
    app.addCountry(country);
    event.preventDefault();
}
            
function removeCountryToSelect(){
    var country = document.getElementById("country").value;
    app.removeCountry(country);
    event.preventDefault();
}