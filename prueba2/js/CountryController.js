function CountryController () {                    
    this.addCountry = function(country) {
        country = country.toLowerCase();
        if(!this.countryExists(country)){
            var select = document.getElementById("selects");
            var newLink = document.createElement('a');
            newLink.setAttribute('href', "www."+country+".com");
            newLink.id = country;
            newLink.innerHTML = country.charAt(0).toUpperCase() + country.slice(1) + " <span class=\"number\">1</span>";
            select.appendChild(newLink);                            
        }
    };
    this.removeCountry = function(country) {
        country = country.toLowerCase();
        document.getElementById(country).remove();                        
    };
    this.countryExists = function(country) {
        var element = document.getElementById(country);
        if(element == null)
            return false;
        else
            return true;
    }
}
                
